![stability-work_in_progress](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)
![issues-open](https://bitbucket.org/repo/ekyaeEE/images/2944199103-issues_open.png)

# RATIONALE #

* This internal project reflects our needs to digitize analogous data to the digital realm.
* There were some insights and failures in the process, but, we reached the goal: not too pricey, technically affordable and easy to transport and deploy.
* This repo is a living document that will grow and adapt over time
 ![NpOymG.gif](https://i.postimg.cc/wMtT8RKp/Untitled.gif)

### What is this repository for? ###

* Quick summary
    - Blueprints and how-to-do a book scanner with a focus on minimalism and few steps
     ![parts.jpg](https://bitbucket.org/repo/98bX9jE/images/1817877143-directories.jpeg)
      ![3d_clamps](https://bitbucket.org/repo/98bX9jE/images/2311216624-clamp.jpeg)
      ![pattern](https://bitbucket.org/repo/98bX9jE/images/1933612315-1.jpg)
     
* Version 1.01

### How do I get set up? ###

* Configuration
    - Check [colophon.md](https://bitbucket.org/imhicihu/book-scanner/src/master/Colophon.md)
* Deployment instructions
    - Check [procedures.md](https://bitbucket.org/imhicihu/book-scanner/src/master/procedures.md)
    - Check [checklist.md](https://bitbucket.org/imhicihu/book-scanner/src/master/checklist.md)
    - Data related: [Bibliography.md](https://bitbucket.org/imhicihu/book-scanner/src/master/Bibliography.md)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/book-scanner/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/book-scanner/commits/) section for the current status

### Contribution guidelines ###

* Code review
    - Pull request for any typo, request a feature, etc. are welcome

### Related repositories ###

* Some repositories linked with this project:
     - [Incunnabilia (early book)- Digitization](https://bitbucket.org/imhicihu/incunnabilia-early-book-digitization/src/)
     - [Digitalizacion (worflow)](https://bitbucket.org/imhicihu/digitalizacion-worflow/src/)
     
### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/book-scanner/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/book-scanner/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### License ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png)